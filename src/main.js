import Vue from 'vue'
import App from './App.vue'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import './style/index.scss'

import VueClipboard from 'vue-clipboard2'
import i18n from './local/index'

Vue.config.productionTip = false

// 注入剪切板
Vue.use(VueClipboard)
// 注入Element-UI
Vue.use(ElementUI)

new Vue({
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
