export default {
  bpmn: {
    actions: {
      import: "导入",
      importXml: "导出XML",
      importSvg: "导出SVG",
      preview: "预览",
      clear: "清除",
      save: "保存",
      close: "关闭"
    },
    tools: {
      startEvent: "启动事件",
      task: "任务",
      userTask: "用户任务",
      serviceTask: '服务任务',
      endEvent: '结束事件'
    },
    property: {
      process: {
        tabName: '流程属性'
      },
      node: {
        tabName: '节点属性'
      }
    }
  }
}