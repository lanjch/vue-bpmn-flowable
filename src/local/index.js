import Vue from "vue";
import VueI18n from 'vue-i18n'
import zh_CN from './lang/zh_CN'
import en_US from "./lang/en_US";

Vue.use(VueI18n)


export default new VueI18n({
  locale: 'zh', // 定义默认语言为中文
  messages: {
    'zh': zh_CN,
    'en': en_US
  }
});